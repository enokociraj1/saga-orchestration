package com.saga.saga.service;

public enum WorkflowStepStatus {
    PENDING,
    COMPLETE,
    FAILED;
}
