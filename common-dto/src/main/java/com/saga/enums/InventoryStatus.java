package com.saga.enums;

public enum  InventoryStatus {
    AVAILABLE,
    UNAVAILABLE;
}
